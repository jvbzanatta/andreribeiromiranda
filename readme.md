Tem WI-FI?
==========

Profissionais que precisam de um **lugar apropriado para trabalhar**, ou seja, pessoas que trabalham remotamente, que viajam, profissionais liberais, pessoas que gostam de variar o seu local de trabalho etc.

## Entregas

Esse projeto foi divido em duas entregas.
Os prazos podem ser vistos no [link](https://docs.google.com/spreadsheets/d/1CWQf5NAh8sQmh4I-2VKbB_nuPUZL_fb2yEu9lD14Jx8/edit?usp=sharing) e as horas trabalhadas no [link](https://docs.google.com/spreadsheets/d/1oI1_2Vy2-LX9o7rcDvrf9gi6ox14emkW-REQkku8zKQ/edit?usp=sharing). 

A primeira entrega está disponível no [link](https://temwifi.miranda.work).

#### Primeira Entrega

A primeira entrega o foco principal foi nas funcionalidades da plataforma.
Foi desenvolvido o Layout, Home, Lista de avaliações na tela principal, Minhas avaliações e visualização das avaliações.


#### Segunda Entrega

A segunda entrega será o cadastro do estabelecimento, cadastro de usuário com autenticação e pagina sobre.

## Instalação da plataforma

A plataforma foi desenvolvida utilizando as tecnologias:
* PHP com o framework Laravel e o ORM Doctrine;
* Frontend com HTML e Vue.js;
* Conteiners com Docker em conjunto com docker-compose.

Necessário ter pré-instalado o Docker e docker-compose. 
Na [documentação oficial](https://docs.docker.com/) tem explicações de como instalar essas ferramentas e na internet tem uma grande variedade de tutorias de como realizar essa instalação.

Com o docker-compose instalado agora iremos iniciar os serviços: nginx, phpfpm e PostgreSQL.

```bash
git clone http://git.vibbra.com.br/candidatos/AndreRibeiroMiranda.git temwifi
cd temwifi
docker-compose up --build
```

Agora podemos fazer a instalação das dependências do PHP, carregar a estrutura do banco de dados e iniciar o carregamento dos dados de teste. 
```bash
docker-compose exec backend bash
composer.phar install
php artisan doctrine:migrations:migrate
php artisan db:seed
```

#### Testes

O cadastro de usuário e autenticação não foi desenvolvido, mas para realização dos testes da pagina Minhas Avaliações foi fixado o usuário com id 1 como usuário logado.
