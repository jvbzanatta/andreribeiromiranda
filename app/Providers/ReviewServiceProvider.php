<?php

namespace App\Providers;

use App\Entities\Establishment;
use App\Entities\User;
use App\Services\ReviewService;
use App\Services\ReviewServiceInterface;
use EntityManager;
use Illuminate\Support\ServiceProvider;

class ReviewServiceProvider extends ServiceProvider
{
    /**
     *
     */
    public function register(): void
    {
        $this->app->bind(ReviewServiceInterface::class, function ($app) {
            $user = EntityManager::getRepository(User::class)->findOneById(1);
            $repositoryEstablishment = EntityManager::getRepository(Establishment::class);
            return new ReviewService($repositoryEstablishment, $user);
        });
    }
}
