<?php
namespace App\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use App\Entities\User;
use App\Entities\Establishment;

/**
 * @ORM\Entity
 * @ORM\Table(name="review")
 * @ORM\HasLifecycleCallbacks
 */
class Review {

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\Column(type="integer")
     * @var int
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    protected $wifi;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    protected $food;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    protected $drink;

    /**
     * @ORM\Column(type="integer")
     * @var string
     */
    protected $attendance;

    /**
     * @ORM\Column(type="integer")
     * @var string
     */
    protected $price;

    /**
     * @ORM\Column(type="integer")
     * @var string
     */
    protected $confort;

    /**
     * @ORM\Column(type="integer")
     * @var string
     */
    protected $noise;

    /**
     * @ORM\Column(type="integer")
     * @var string
     */
    protected $raitingGeneral;

    /**
     * @ORM\Column(type="datetime")
     * @var DateTime
     */
    protected $reviewDateCreated;

    /**
     * @ORM\Column(type="datetime")
     * @var DateTime
     */
    protected $reviewDateUpdate;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Establishment", inversedBy="reviews")
     * @ORM\JoinColumn(name="establishment_id", referencedColumnName="id")
     * @var App\Entities\Establishment
     */
    protected $establishment;

    /**
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="reviews")
     * @ORM\JoinColumn(name="reviewer_id", referencedColumnName="id")
     * @var App\Entities\User
     */
    protected $reviewer;

    /**
     * @ORM\PrePersist
     */
    public function dateReviewCreated(): void
    {
        $this->reviewDateUpdate = $this->reviewDateCreated = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function dateReviewUpdated(): void
    {
        $this->reviewDateUpdate = new \DateTime();
    }

    public function __construct()
    {
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }


    /**
     * @return string
     */
    public function getWifi(): string
    {
        return $this->wifi;
    }

    /**
     * @return string
     */
    public function getFood(): string
    {
        return $this->food;
    }

    /**
     * @return string
     */
    public function getDrink(): string
    {
        return $this->drink;
    }

    /**
     * @return string
     */
    public function getPrice(): string
    {
        return $this->price;
    }

    /**
     * @return string
     */
    public function getConfort(): string
    {
        return $this->confort;
    }

    /**
     * @return string
     */
    public function getNoise(): string
    {
        return $this->noise;
    }

    /**
     * @return string
     */
    public function getRaitingGeneral(): string
    {
        return $this->raitingGeneral;
    }

    /**
     * @return string
     */
    public function getAttendance(): string
    {
        return $this->attendance;
    }

    /**
     * @return Establishment
     */
    public function getEstablishment(): Establishment
    {
        return $this->establishment;
    }

    /**
     * @param Establishment $establishment
     */
    public function setEstablishment(Establishment $establishment): void
    {
        $this->establishment = $establishment;
    }

    /**
     * @return App\Entities\User
     */
    public function getReviewer(): App\Entities\User
    {
        return $this->reviewer;
    }

    /**
     * @param App\Entities\User $reviewer
     */
    public function setReviewer(User $reviewer): void
    {
        $this->reviewer = $reviewer;
        $reviewer->addReview($this);
    }

}
