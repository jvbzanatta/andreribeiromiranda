<?php
namespace App\Entities;

use Doctrine\ORM\EntityRepository;

class EstablishmentRepository extends EntityRepository
{
    public function search(string $search, int $price = null, int $id = null): array
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $queryBuilder = $qb->select('e')
            ->from(Establishment::class, 'e')
            ->innerJoin('e.reviews', 'r');

        if (!empty($search)) {
            $search = strtolower($search);
            $queryBuilder->where($qb->expr()->like('LOWER(e.name)', ':search'))
                ->orWhere($qb->expr()->like('LOWER(e.type)', ':search'))
                ->orWhere($qb->expr()->like('LOWER(e.address)', ':search'))
                ->orWhere($qb->expr()->like('LOWER(e.city)', ':search'))
                ->orWhere($qb->expr()->like('LOWER(e.state)', ':search'))
                ->orWhere($qb->expr()->like('LOWER(e.country)', ':search'))
                ->setParameter("search", "%{$search}%");
        }

        if (! empty($price)) {

            $queryBuilder->andWhere('r.price = :price')
                ->setParameter("price", $price);
        }

        if (! empty($id)) {
            $queryBuilder->innerJoin('e.reviewer', 'u_e')
                ->innerJoin('r.reviewer', 'u_r')
                ->andWhere('u_e.id = :reviewer or u_r.id = :reviewer')
                ->setParameter("reviewer", $id);
        }

        return $queryBuilder->getQuery()->getResult();
    }
}
