<?php
namespace App\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Illuminate\Container\RewindableGenerator;


/**
 * @ORM\Entity
 * @ORM\Table(name="users")
 */
class User
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\Column(type="integer")
     * @var int
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    protected $name;


    /**
     * @ORM\OneToMany(targetEntity="Establishment", mappedBy="reviewer")
     * @var App\Entities\Establishment[]
     */
    protected $establishments;

    /**
     * @ORM\OneToMany(targetEntity="Review", mappedBy="reviewer")
     * @var App\Entities\Review[]
     */
    protected $reviews;

    public function __construct()
    {
        $this->establishments = new ArrayCollection();
        $this->reviews = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    public function addEstablishment(Establishment $establishment): void
    {
        $this->establishments->add($establishment);
    }

    public function addReview(Review $review): void
    {
        $this->reviews->add($review);
    }

}
