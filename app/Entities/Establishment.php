<?php
namespace App\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Illuminate\Contracts\Support\Arrayable;
use Prophecy\Exception\Doubler\MethodNotFoundException;

/**
 * @ORM\Entity(repositoryClass="EstablishmentRepository")
 * @ORM\Table(name="establishments")
 */
class Establishment implements Arrayable
{

    const TYPES = [
        'Café',
        'Restaurante',
        'Coworking',
        'Livraria',
        'Outro',
    ];

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\Column(type="integer")
     * @var int
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    protected $name = '';

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    protected $type = '';

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    protected $address = '';

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    protected $city = '';

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    protected $state = '';

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    protected $country = '';

    /**
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="establishments")
     * @ORM\JoinColumn(name="reviewer_id", referencedColumnName="id")
     * @var App\Entities\User
     */
    protected $reviewer = null;

    /**
     * @ORM\OneToMany(targetEntity="Review", mappedBy="establishment", cascade={"persist"})
     * @ORM\OrderBy({"reviewDateUpdate" = "DESC"})
     * @var App\Entities\Review[]
     */
    protected $reviews;

    private $ratingMeans = [];

    private $reviewJoin = [];

    public function __construct()
    {
        $this->reviews = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param Review $review
     */
    public function addReview(Review $review): void
    {
        $this->reviews->add($review);
        $review->setEstablishment($this);
    }

    /**
     * @return Review[]
     */
    public function getReviews(): Collection
    {
        return $this->reviews;
    }

    /**
     * @param Review[] $reviews
     */
    public function setReviews(array $reviews): void
    {
        $this->reviews->clear();
        foreach($reviews as $review) {
            $this->addReview($review);
        }
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @return string
     */
    public function getState(): string
    {
        return $this->state;
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }

    /**
     * @return App\Entities\User
     */
    public function getReviewer(): User
    {
        return $this->reviewer;
    }

    /**
     * @param App\Entities\User $reviewer
     */
    public function setReviewer(User $reviewer): void
    {
        $this->reviewer = $reviewer;
        $reviewer->addEstablishment($this);
    }

    /**
     * @return string
     */
    public function wifi(): string
    {
        $outputs = '';
        foreach ($this->reviews as $review) {
            $outputs .= $review->getWifi();
        }
        return $outputs;
    }

    public function joinReview(string $key = null): string
    {
        return $this->joinReviews([$key])[$key];
    }

    public function joinReviews(array $key = []): array
    {
        $this->prepareJoinReviews($key);
        return $this->reviewJoin;
    }

    private function prepareJoinReviews(array $only = []): void
    {
        $parameters_valid = [
            'wifi',
            'food',
            'drink',
        ];

        if (empty($only) ) {
            $parameters = $parameters_valid;
        } else {
            $parameters = array_intersect($parameters_valid, $only);
        }

        $outputs = [];
        foreach ($parameters as $parameter) {
            $outputs[$parameter] = [];
        }

        $reviews = $this->getReviews();
        foreach ($reviews as $review) {
            foreach ($parameters as $param) {
                $method = 'get' . ucfirst($param);
                if (! method_exists($review, $method)) {
                    throw new \RuntimeException($method);
                }

                $outputs[$param][] = $review->{$method}();
            }
        }

        foreach ($parameters as $param) {
            $outputs[$param] = implode(', ', $outputs[$param]);
        }

        $this->reviewJoin = $outputs;
    }

    /**
     * @param string|null $key
     * @return string
     */
    public function rating(string $key = null): string
    {
        return (string) $this->ratings([$key])[$key];
    }

    /**
     * @param array $key
     * @return array
     */
    public function ratings(array $key = []): array
    {
        $this->calculateReviewMean($key);
        return $this->ratingMeans;
    }

    private function calculateReviewMean(array $only = []): void
    {
        $parameters_valid = [
            'price',
            'attendance',
            'confort',
            'noise',
            'raitingGeneral',
        ];

        if (empty($only) ) {
            $parameters = $parameters_valid;
        } else {
            $parameters = array_intersect($parameters_valid, $only);
        }

        $means = [];
        foreach ($parameters as $parameter) {
            $means[$parameter] = 0;
        }

        $reviews = $this->getReviews();
        foreach ($reviews as $review) {
            foreach ($parameters as $param) {
                $method = 'get' . ucfirst($param);
                if (! method_exists($review, $method)) {
                    throw new \RuntimeException($method);
                }

                $means[$param] += $review->{$method}();
            }
        }

        $countReviews = count($reviews);
        foreach ($parameters as $param) {
            $means[$param] = round($means[$param] / $countReviews, 2);
        }
        $this->ratingMeans = $means;
    }

    public function getCompleteAddress(): string
    {
        return implode([
            $this->getAddress(),
            $this->getCity(),
            $this->getState(),
            $this->getCountry(),
        ], ', ');
    }

    /**
     * Get the instance as an array.
     *
     * @return array
     */
    public function toArray(): array
    {
        $ratings = $this->ratings();
        $joinReviews = $this->joinReviews();
        return array_merge([
            'id' => $this->getId(),
        ], $ratings, $joinReviews);
    }
}
