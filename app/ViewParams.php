<?php

namespace App;

use App\Entities\Establishment;
use Doctrine\Common\Collections\Collection;

trait ViewParams {

    protected function viewHome(array $establishments, bool $makeReview = false, string $search = '', int $price = 0, string $url = '/establishment/search')
    {
        return view('home', [
            'establishments' => $establishments,
            'search' => $search,
            'price' => $price,
            'make_review' => $makeReview,
            'search_url' => url($url)
        ]);
    }

    protected function viewAbout()
    {
        return view('about');
    }

    protected function viewReview(bool $edit, Establishment $establishment = null)
    {
        return view('review', ['edit' => $edit, 'establishment' => $establishment]);
    }

}
