<?php

namespace App\Http\Controllers;

use App\Entities\Establishment;
use App\Http\Controllers\Controller;
use App\Services\ReviewServiceInterface;
use App\ViewParams;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class MyReviewsController extends Controller
{

    use ViewParams;

    private $reviewService;

    public function __construct(ReviewServiceInterface $reviewService)
    {
        $this->reviewService = $reviewService;
    }

    public function list()
    {
        $establishments = $this->reviewService->search('',0, true);
        return $this->viewHome($establishments, false, '', 0, '/my-reviews/search');
    }

    public function search(Request $request)
    {
        $search = $request->get('search', '');
        $price = $request->get('price', 0);

        if ($search === null) {
            $search = '';
        }

        $establishments = $this->reviewService->search($search, (int) $price, true);
        return $this->viewHome($establishments, false, $search, $price, '/my-reviews/search');
    }

    public function create()
    {
        return $this->viewReview(true, new Establishment());
    }

    public function edit($id)
    {
        $establishment = $this->reviewService->getEstablishment($id);
        return $this->viewReview(true, $establishment);
    }

    public function save()
    {
        return redirect('/establishment/edit/' + 1);
    }

    public function _viewReview()
    {

    }


}
