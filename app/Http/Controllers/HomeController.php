<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Services\ReviewService;
use App\Services\ReviewServiceInterface;
use App\ViewParams;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    use ViewParams;

    private $reviewService;

    public function __construct(ReviewServiceInterface $reviewService)
    {
        $this->reviewService = $reviewService;
    }

    public function home()
    {
        $establishments = $this->reviewService->search();
        return $this->viewHome($establishments, true);
    }

    public function about()
    {
        return $this->viewAbout();
    }

    public function search(Request $request)
    {
        $search = $request->get('search', '');
        $price = $request->get('price', 0);

        if ($search === null) {
            $search = '';
        }

        $establishments = $this->reviewService->search($search, (int) $price, false);
        return $this->viewHome($establishments, true, $search, $price);
    }

    public function info(int $id)
    {
        return $this->reviewService->getEstablishment($id);
    }

}
