<?php

namespace App\Services;

use App\Entities\Establishment;
use Doctrine\Common\Collections\Collection;


interface ReviewServiceInterface
{
    /**
     * @param string $search
     * @param int $price
     * @param bool $myReviews
     * @return array
     */
    public function search(string $search = '', int $price = 0, bool $myReviews = false): array;

    /**
     * @param int $id
     * @return Tool|null
     */
    public function getEstablishment(int $id): ?Establishment;

    /**
     * @param string $title
     * @param string $link
     * @param string $description
     * @param array $tags
     * @return Tool
     */
    public function addEstablishment(string $title, string $link, string $description, array $tags): Establishment;

    /**
     * @param int $id
     * @return bool
     */
    public function deleteEstablishment(int $id): bool;

    public function reviews(Establishment $establishment): array;

}
