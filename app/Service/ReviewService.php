<?php

namespace App\Services;


use App\Entities\Establishment;
use App\Entities\EstablishmentRepository;
use App\Entities\User;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityRepository;
use EntityManager;


final class ReviewService implements ReviewServiceInterface
{

    /**
     * @var EstablishmentRepository
     */
    private $establishmentRepository;

    /**
     * @var User
     */
    private $user;

    /**
     * ReviewService constructor.
     *
     * @param EntityRepository $establishmentRepository
     * @param User $user
     */
    public function __construct(EntityRepository $establishmentRepository, User $user = null)
    {
        $this->establishmentRepository = $establishmentRepository;
        $this->user = $user;
    }

    public function search(string $search = '', int $price = 0, bool $myReviews = false): array
    {
        $id = null;
        if ($myReviews and $this->user !== null) {
            $id = $this->user->getId();
        }
        return $this->establishmentRepository->search($search, $price, $id);
    }


    /**
     * @param int $id
     * @return Establishment|null
     */
    public function getEstablishment(int $id): ?Establishment
    {
        return $this->establishmentRepository->findOneById($id);
    }

    /**
     * @param string $title
     * @param string $link
     * @param string $description
     * @param array $tags
     * @return Establishment
     */
    public function addEstablishment(string $title, string $link, string $description, array $tags): Establishment
    {
        // TODO: Implement addEstablishment() method.
    }

    /**
     * @param int $id
     * @return bool
     */
    public function deleteEstablishment(int $id): bool
    {
        // TODO: Implement deleteEstablishment() method.
    }

    public function reviews(Establishment $establishment): array
    {
        // TODO: Implement reviews() method.
    }
}
