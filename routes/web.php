<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['uses' => 'HomeController@home'])->name('home');;
Route::get('/establishment/search', ['uses' => 'HomeController@search'])->name('establishment_search');
Route::get('/establishment/info/{id}', ['uses' => 'HomeController@info'])->name('establishment_info');

Route::get('/my-reviews', ['uses' => 'MyReviewsController@list'])->name('my-reviews');
Route::get('/my-reviews/search', ['uses' => 'MyReviewsController@search'])->name('my-reviews_search');
Route::get('/establishment/create', ['uses' => 'MyReviewsController@create'])->name('establishment_create');
Route::get('/establishment/edit/{id}', ['uses' => 'MyReviewsController@edit'])->name('establishment_edit');
Route::post('/establishment/create', ['uses' => 'MyReviewsController@save'])->name('establishment_save');
Route::get('/establishment/delete/{id}', ['uses' => 'MyReviewsController@delete'])->name('establishment_delete');

Route::get('/about', ['uses' => 'HomeController@about'])->name('about');

Route::post('login', [ 'as' => 'login', 'uses' => 'LoginController@do']);

