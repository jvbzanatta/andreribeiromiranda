<?php

use App\Entities\Establishment;
use App\Entities\Review;
use App\Entities\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{

    const MAX_USERS = 10;
    const MAX_REVIEW = 5;
    const COUNT_ESTABLISHMENT = 50;

    private $em;

    private $reviewer = [];

    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->createEstablishment();
        // $this->call(UsersTableSeeder::class);
    }

    private function createEstablishment()
    {
        entity(Establishment::class, self::COUNT_ESTABLISHMENT)->create()
            ->each(function (Establishment $establishment) {
                $establishment->setReviewer($this->createReviewer());
                $r = $this->createReview();
                $establishment->setReviews($r);

                $this->em->persist($r[0]);
                $this->em->persist($establishment);
            });
        $this->em->flush();
    }

    private function createReview()
    {
        $reviews = [];
        $number = rand(2, self::MAX_REVIEW);
        entity(Review::class, $number)->create()
            ->each(function (Review $review) use (&$reviews) {
                $review->setReviewer($this->createReviewer());
                $this->em->persist($review);
                $reviews[] = $review;
            });
        return $reviews;
    }

    private function createReviewer()
    {
        if (count($this->reviewer) >= self::MAX_USERS) {
            $user = rand(0, self::MAX_USERS - 1);
            return $this->reviewer[$user];
        }

        $user = entity(User::class, 1)->create();
        $this->em->persist($user);
        $this->reviewer[] = $user;
        return $user;
    }
}
