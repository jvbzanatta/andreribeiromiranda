<?php

use App\Entities\Establishment;
use App\Entities\Review;
use App\Entities\User;
use Faker\Generator;


$factory->define(Establishment::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->unique()->name,
        'type' => Establishment::TYPES[rand(0, count(Establishment::TYPES)-1)],
        'address' => $faker->streetAddress,
        'city' => $faker->city,
        'state' => $faker->streetName,
        'country' => $faker->country,
    ];
});

$factory->define(Review::class, function (Faker\Generator $faker) {
    return [
        'wifi' => $faker->unique()->name,
        'food' => $faker->unique()->name,
        'drink' => $faker->unique()->name,
        'attendance' => rand(0, 5),
        'price' => rand(1, 5),
        'confort' => rand(1, 5),
        'noise' => rand(1, 5),
        'raitingGeneral' => rand(1, 5),
    ];
});

$factory->define(User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->unique()->name,
    ];
});

