@extends('layouts.main')

@section('title', 'Home')

@section('content')

<div class="album py-5 bg-light">
    <div class="container">
        @if ($edit)
            @include('reviews.form')
        @else
            @include('reviews.view')
        @endif
    </div>
</div>

@endsection
