




<div class="album py-5 bg-light">
    <div class="container" id="establishemnts">
        <div class="row">
            @foreach ($establishments as $establishment)
                <div class="col-md-4">
                    <div class="card mb-4 shadow-sm">
                        <a class="show" style="cursor: pointer;" v-on:click="establishementShow({{ $establishment->getId() }})">
                            <svg class="bd-placeholder-img card-img-top" width="100%" height="225" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: Thumbnail"><title>Placeholder</title><rect width="100%" height="100%" fill="#55595c"/><text x="50%" y="50%" fill="#eceeef" dy=".3em">
                            </text>
                            </svg>
                        </a>
                        <div class="card-body">
                            <p class="card-text text-center">
                                {{ $establishment->getName() }} ({{ $establishment->getType() }})
                            </p>
                            <p class="card-text">
                                <strong>Nota Geral:</strong> {{ $establishment->rating('raitingGeneral') }}<br>
                                <strong>Localização:</strong> {{ $establishment->getCompleteAddress() }}<br>
                                <strong>WI-FI:</strong> {{ $establishment->joinReview('wifi') }} <br>
                            </p>
                            <establishment-show v-bind:id="{{ $establishment->getId() }}"
                                                v-bind:data="data"></establishment-show>
                            <div class="d-flex justify-content-end">
                                @if ($make_review)
                                <a href="{{ url('/establishment/edit/') }}/{{ $establishment->getId() }}" class="btn btn-sm btn-outline-secondary">
                                    <i class="fa fa-plus"></i>
                                </a>
                                @else
                                <a href="{{ url('/establishment/edit/') }}/{{ $establishment->getId() }}" class="btn btn-sm btn-outline-secondary">
                                    <i class="fa fa-edit"></i>
                                </a>
                                @endif


                            </div>
                        </div>

                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>

