@extends('layouts.main')

@section('title', 'Home')


@section('content')
@include('layouts.search')

<div class="album py-5 bg-light">
    <div class="container">
        @include('reviews.list')
    </div>
</div>

@endsection
