<section class="jumbotron">
    <div class="container">
        <h1 class="jumbotron-heading text-center">Encontre o melhor lugar para realizar o seu trabalho</h1>

        <div class="search">
            <form class="search"
                  method="get" action="{{ $search_url }}">
                <div class="form-group">
                    <label for="search">Busca por Nome, Tipo ou Localização</label>
                    <input type="search"
                           placeholder="Nome, Tipo, Localização"
                           aria-label="Nome, Tipo, Localização"
                           class="form-control"
                           name="search"
                           id="search"
                           value="{{ $search }}">
                </div>
                <div class="form-group">
                    <label for="price">Busca por Preço</label>
                    <select class="form-control" name="price" id="price">
                        <option value="0"></option>
                        @for ($i=1; $i<=5; $i++)
                        <option value="{{ $i }}" @if ($price == $i) selected @endif>{{ $i }}</option>
                        @endfor
                    </select>
                </div>
                <div class="d-flex justify-content-end">
                    <button class="btn btn-outline-success p-2" type="submit">Encontrar Local</button>
                </div>
            </form>
        </div>
    </div>
</section>
