<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Tem WI-FI?">

    <title>@yield('title') - Tem WI-FI?</title>

    <link href="{{ asset('css/app.css') }}" rel="stylesheet"/>
</head>
<body>

<header>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container d-flex justify-content-between">
            <a href="/" class="navbar-brand d-flex align-items-center">
                <img class="d-inline-block align-top logo" src="{{ asset('img/logo.png') }}">
                <strong>Tem WI-FI?</strong>
            </a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
                    aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Alterna navegação">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav">
                    <a class="nav-item nav-link @if (@Route::currentRouteName() == 'my-reviews') active @endif"
                       href=" {{ url('/my-reviews') }}">Minhas Avaliações</a>
                    <a class="nav-item nav-link @if (@Route::currentRouteName() == 'establishment_create') active @endif"
                       href="{{ url('/establishment/create') }}">Cadastrar Estabelecimento</a>
                    <a class="nav-item nav-link @if (@Route::currentRouteName() == 'about') active @endif"
                       href="{{ url('about') }}">Sobre</a>

                </div>
            </div>

            <a class="nav-item nav-link @if (@Route::currentRouteName() == 'login') active @endif login"
               href="{{ url('login') }}">Login / Signup</a>
            <span class="user">
                <span class="fa-stack fa-1x">
                    <i class="fas fa-circle fa-stack-2x"></i>
                    <i class="fa fa-user fa-stack-1x fa-inverse"></i>
                </span>
            </span>

        </div>
    </nav>
</header>

<main role="main">
    @yield('content')
</main>

<footer class="text-muted">
    <div class="container">
        <p class="float-right">
            <a href="#">Back to top</a>
        </p>
        <p>&copy; 2019 Tem WIFI?</p>
    </div>
</footer>

</body>

<script src="{{ url('js/app.js') }}"></script>

</html>
