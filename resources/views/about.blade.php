@extends('layouts.main')

@section('title', 'Home')

@section('content')

<div class="album py-5 bg-light">
    <div class="container">
        

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam condimentum sollicitudin enim, eu venenatis diam auctor congue. Quisque egestas auctor tortor quis lobortis. In hac habitasse platea dictumst. Suspendisse suscipit pharetra sem sit amet mollis. Curabitur eu diam sem. Duis tincidunt lacus lacus, eget dapibus dolor laoreet non. Proin tempor tortor sed orci dictum porta. Etiam sit amet ante scelerisque, tincidunt leo eu, fermentum justo.</p>

<p>Donec sit amet eleifend justo. Etiam gravida quis nisi non semper. Vivamus ultrices malesuada libero at ultrices. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nam in sapien ac elit vehicula euismod. Phasellus rhoncus sit amet ex nec suscipit. Aliquam molestie tortor in nibh egestas, non interdum ante tempor. Curabitur sagittis libero nec est porttitor aliquet. In eu ex eget ex tempor scelerisque varius at ipsum. Phasellus est tortor, placerat a convallis vel, faucibus vel dolor.</p>

<p>Suspendisse semper sapien ac orci egestas, eu lobortis erat porta. Curabitur in facilisis lorem. Maecenas rutrum tristique mi, quis mattis arcu interdum sit amet. Pellentesque a semper quam. Pellentesque feugiat, orci quis rhoncus sollicitudin, nulla orci consequat diam, id congue tellus purus eget erat. Aenean tincidunt tellus in velit convallis, eget ullamcorper mauris porttitor. Duis porta est pretium porta facilisis. Nullam semper porta nulla quis dignissim. Nunc non rutrum justo. Phasellus quis facilisis leo, in venenatis nisi. In vitae pretium dolor, ut hendrerit risus.</p>

<p>Duis semper ex vitae elementum ultricies. Curabitur in metus ut augue vulputate laoreet. Duis et ipsum dolor. In pharetra purus ipsum, eu semper dolor ultrices id. Vivamus ornare erat ac pellentesque ullamcorper. Nunc ut quam sed velit ullamcorper placerat. Nullam tempor lobortis velit. Duis tempus tincidunt velit sagittis iaculis. Nam lacinia tortor orci, et tincidunt neque hendrerit quis. Suspendisse potenti. Etiam pharetra turpis et dignissim vulputate. Vivamus congue turpis sed libero accumsan, ut feugiat leo luctus.</p>

<p>Cras augue nisl, porttitor nec diam id, hendrerit egestas lorem. Suspendisse congue ut urna sit amet mollis. Ut feugiat, lorem ac ullamcorper rhoncus, odio massa varius metus, sed vulputate purus diam laoreet risus. Pellentesque nulla ante, facilisis congue metus luctus, molestie interdum nisl. Fusce tempus eros est, eget venenatis augue tristique ut. Nunc vitae iaculis leo. Phasellus aliquam nec nisi sit amet scelerisque. </p>

    </div>
</div>

@endsection
